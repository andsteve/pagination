$( document ).ready(function() {
	'use strict';
	var APP = window.APP = window.APP || {};

	// Creates module home to wrap all home view 
	APP.home = (function(){

		// Get the container home div
		var home = document.getElementsByClassName('home')[0];

		// Save data companies
		var data = [];

		// Checks if the app is in the home view
		if(home){
			// Load the information of the companies and save it in the "data" variable
			showCompanies();
		}

		// function gets the full address of the url
		function getFullURL() {
			return document.URL;	
		}

		// Function read the url and get the value of the company
		function getUrlCompanyPage() {
			var url = getFullURL();
			var link = new URL(url);
			var params = new URLSearchParams(link.search.slice(1));

			return params.get('company');
		}

		// function gets the last page of companies
		function getLastPage(data) {
			var lastPage = Math.floor(data.length / 5);

			if(lastPage % 2 !== 0 ){
				lastPage++;
			}
			return lastPage;
		}

		function setUrlCompanyPage(page) {
			
			// Get the full path of the url
			var link = getFullURL();
			
			// create an url
			var url = new URL(link);
			
			//  Get the params of the url
			var params = new URLSearchParams(url.search.slice(1));
			
			params.set('company', page);
			
			window.history.replaceState({}, '','/?company='+page);
		}

		function setFocusPagination(page) {
			var wrapPaginationDesktop = home.getElementsByClassName("wrap-pagination_desktop")[0];
			var itemsContainer = wrapPaginationDesktop.getElementsByClassName("items-container")[0];
			var ul  = itemsContainer.getElementsByTagName("UL")[0];
			var listA = Array.from(ul.getElementsByClassName('item'));
				
			for (var i = 0; i < listA.length; i++) {

				if(page === listA[i].innerHTML){
					listA[i].focus();
				}
			}
		}

		// Function checks if the url has a company property, if does show and hide some tag of the markup 
		function checksIfCompanyExist(url) {
			if(url.indexOf('company') > 0){
				$(".showCompanies").hide();
			}
		}		

		// Function to load the information of the companies and save it in the "data" variable
		function showCompanies() {

			// Get the btn showCompanies
			var btnShowCompany = home.getElementsByClassName("showCompanies")[0];

			// gets full address of the url
			var url = getFullURL();

			var page = '';

			// Function checks if the url has a company property
			checksIfCompanyExist(url);

			// if data is empty execute the request
			if(data.length <= 0){

				// Function makes the request to the server and load the data companies
				requestHTTP(100,1);
			}

			// Creates the event listener for the btn show companies
			btnShowCompany.addEventListener("click", function(){
				
				// This update url and add some new properties
				history.pushState(null, null, "?company=1");

				executeAfterPageLoadDataValues();

				btnShowCompany.style.display='none';
			});

			// Function creates the link to do the request and execute the callback to load the data companies
			function requestHTTP(number_of_items, start){
				//'http://api.joind.in/v2.1/events?resultsperpage=100&start=170'
				
				// gets the full address of the url
				var url = getFullURL();
				
				// Link to the request
				var request = 'http://api.joind.in/v2.1/events?resultsperpage='+number_of_items+'&start='+start;

				// Function does the request to the server and load the data companies
				callAjax(url, request, function(value, url) {

					// Set the data companies
					data = value;

					// Checks if the current url has the company property
					if(url.indexOf('company') > 0){
						executeAfterPageLoadDataValues();
					}
				});

				// Function does the request to the serve and execute the callback
				function callAjax(url, request, callback) {
					$.ajax({
						method: "GET",
						url: request,
						data: {}
					})
					.done(function(values, stauts,test) {

						callback(values.events, url);
					});
				};
			};	
		};

		// Function create the pagination and append it to the markup
		function createPagination(data) {
			
			var wrapPagination = home.getElementsByClassName("wrap-pagination")[0];
			
			createMobilePagination();
			createDesktopPagination();
			createPreviosAndNextDesktopPagination(data);

			// Function creates the mobile pagination
			function createMobilePagination() {

				updateMobilePagination();
			}

			// Function creates the desktop pagination
			function createDesktopPagination() {

				var wrapPaginationDesktop = wrapPagination.getElementsByClassName('wrap-pagination_desktop')[0];
				var itemsContainer = wrapPaginationDesktop.getElementsByClassName('items-container')[0];
				var ul = itemsContainer.getElementsByTagName('UL')[0];

				// Creates all items and append them to the markup
				for (var i = 0; i < 4; i++) {
					var li = document.createElement("LI");
					var a = document.createElement("A");

					a.innerHTML = i+1;
					a.href="#";
					a.classList.add("item");
					li.append(a);
					ul.append(li);
				}
			}

			// Funtion sets the first page and the last page of the desktop pagination
			function createPreviosAndNextDesktopPagination(data) {
				var wrapPaginationDesktop = wrapPagination.getElementsByClassName('wrap-pagination_desktop')[0];
				var wrapPaginationDesktopPrevious = wrapPagination.getElementsByClassName('wrap-pagination-desktop_previous')[0];
				var wrapPaginationDesktopNext = wrapPagination.getElementsByClassName('wrap-pagination-desktop_next')[0];
				var previosItem = wrapPaginationDesktopPrevious.getElementsByClassName('item')[0];
				var nextItem = wrapPaginationDesktopNext.getElementsByClassName('item')[0];

				var lastPage = getLastPage(data);

				previosItem.innerHTML = '1';
				nextItem.innerHTML = lastPage;
			}
		}


		function hidePartsOfThePagination(page) {

			var wrapPagination = home.getElementsByClassName("wrap-pagination")[0];
			var wrapPaginationDesktop = wrapPagination.getElementsByClassName("wrap-pagination_desktop")[0];
			var wrapPaginationMobile = wrapPagination.getElementsByClassName("wrap-pagination_mobile")[0];

			//Desktop
			var wrapPaginationDesktopPrevious = wrapPaginationDesktop.getElementsByClassName("wrap-pagination-desktop_previous")[0];
			var wrapPaginationDesktopNext = wrapPaginationDesktop.getElementsByClassName("wrap-pagination-desktop_next")[0];
			var itemsContainer = wrapPaginationDesktop.getElementsByClassName("items-container")[0];

			//Mobile
			var btnMobilePrevious = wrapPaginationMobile.getElementsByClassName("btn-previous")[0];
			var btnMobileNext = wrapPaginationMobile.getElementsByClassName("btn-next")[0];
			

			var lastPage = getLastPage(data);

			if(page > 3 && page < lastPage-1){
				wrapPaginationDesktopPrevious.style.display = "block";
				wrapPaginationDesktopNext.style.display = "block";
				btnMobilePrevious.style.display = "block";
				btnMobileNext.style.display = "block";

			}else{

				if(page < 4){
					wrapPaginationDesktopPrevious.style.display = "none";
					btnMobilePrevious.style.display = "none";

					wrapPaginationDesktopNext.style.display = "block";
					btnMobileNext.style.display = "block";
				}else{
					wrapPaginationDesktopNext.style.display = "none";
					btnMobileNext.style.display = "none";

					wrapPaginationDesktopPrevious.style.display = "block";
					btnMobilePrevious.style.display = "block";
				}
			}
		}	

		// Funtion receives the value of the page in the current url and returns the data from that page
		function displayData(page) {

			var numberPage = 0;

			// Checks if the campany value has a zero if does, redirect to home
			if(page === '0'){

				$(location).attr('href','http://localhost:7000');
			}

			// To the current page value subtract one
			numberPage = (page - 1);

			// Get a slice from the data companies with the information of the current page 
			var values = data.slice(numberPage * 5, (numberPage * 5) + 5);

			// Return the information
			return values;	
		};		

		// Function creates the event listener for the item of the pagination
		function pagination() {

			// Get the wrap pagination container
			var wrapPagination = home.getElementsByClassName("wrap-pagination")[0];
			var wrapPaginationDesktop = wrapPagination.getElementsByClassName("wrap-pagination_desktop")[0];
			var itemsContainer = wrapPaginationDesktop.getElementsByClassName("items-container")[0];
			var ul = itemsContainer.getElementsByTagName("UL")[0];
			// Get the full path of the url
			var link = getFullURL();

			// create an url
			var url = new URL(link);

			// Creates the event listener for the items
			ul.addEventListener('click',function() {

				var target = event.target;
				var page = '';
				var values = [];
				var pageUpdated = '';

				// Checks if the target has the class item
				if ( target.className === 'item') {
					page = Number(target.innerHTML);
					refreshAfterUrlChange(page);
				}
			});
		};


		function previousAndNextMobilePagination() {
			var wrapPagination = home.getElementsByClassName("wrap-pagination")[0];
			var mobilePagination =  wrapPagination.getElementsByClassName("wrap-pagination_mobile")[0];
			var btnPrevious = mobilePagination.getElementsByClassName("btn-previous")[0];
			var btnNext = mobilePagination.getElementsByClassName("btn-next")[0];

			mobilePagination.addEventListener('click',function() {

				var page = Number(getUrlCompanyPage());

				if(event.target.tagName === 'A'){

					if(event.target.className === 'btn-previous'){

						page--;
					}else{

						page++;
					}
					refreshAfterUrlChange(page);
				}
				return false;
			});
		}

		function previousAndNextDesktopPagination() {
			var wrapPagination = home.getElementsByClassName("wrap-pagination")[0];
			var desktopPagination =  wrapPagination.getElementsByClassName("wrap-pagination_desktop")[0];
			var wrapPaginationDesktopPrevious = desktopPagination.getElementsByClassName("wrap-pagination-desktop_previous")[0];
			var wrapPaginationDesktopNext = desktopPagination.getElementsByClassName("wrap-pagination-desktop_next")[0];
			
			var btnPrevious = wrapPaginationDesktopPrevious.getElementsByClassName("btn-previous")[0];
			var btnNext = wrapPaginationDesktopNext.getElementsByClassName("btn-next")[0];

			var lastPage = getLastPage(data);

			desktopPagination.addEventListener('click', function() {
				var page = Number(getUrlCompanyPage());

				if(event.target.tagName === 'A'){

					if(event.target.parentNode.className === 'wrap-pagination-desktop_previous'){
						
						if(event.target.className === 'btn-previous'){
							page--;
						}else{
							page = 1;
						}
						refreshAfterUrlChange(page);
					}else{

						if (event.target.parentNode.className === 'wrap-pagination-desktop_next') {

							if(event.target.className === 'btn-next'){
								page++;
							}else{
								page = lastPage;
							}

							refreshAfterUrlChange(page);
						}
					}
				}
			});
		}

		// Funtion executes some functione ones the data have been loaded
		function executeAfterPageLoadDataValues() {
			
			var page = '';
			var values = [];

			// creates the event listener for the items of the pagination
			pagination();
			
			// Creates the mobile and desktop pagination and append the all of the to the markup
			createPagination(data);

			// shows the wrap pagination
			$('.wrap-pagination').show();

			previousAndNextMobilePagination();
			previousAndNextDesktopPagination();

			// Get the value of the company in the url
			page = getUrlCompanyPage();

			// Get data companies of the current page
			values = displayData(page);

			// Load and display to the markup the parsed information 
			loadCompanyData(values);

			// var page = getUrlCompanyPage();
			updateDesktopPagination();
			setFocusPagination(page);

			hidePartsOfThePagination(page);
		}


		// Functtion updates the mobile pagination
		function updateMobilePagination() {

			var wrapPaginationMobile = home.getElementsByClassName("wrap-pagination_mobile")[0];
			var span = wrapPaginationMobile.getElementsByTagName('SPAN')[0];
			var urlPage = getUrlCompanyPage();
			var lastPage = getLastPage(data);

			span.innerHTML = urlPage.concat(' of ').concat(lastPage);
		}

		// Functtion updates the desktop pagination
		function updateDesktopPagination(){
			
			var wrapPaginationDesktop = home.getElementsByClassName("wrap-pagination_desktop")[0];
			var itemsContainer = wrapPaginationDesktop.getElementsByClassName("items-container")[0];
			var ul  = itemsContainer.getElementsByTagName("UL")[0];
			var listA = Array.from(ul.getElementsByClassName('item'));

			var urlPage = Number(getUrlCompanyPage());
			var lastPage = getLastPage(data);

			if(urlPage < 4){
				pageLessThanFour(urlPage);
			}else{
				if(lastPage === urlPage || (lastPage - 1) === urlPage){
					pageAlmostAtTheEnd(urlPage, lastPage);
				}else{
					if( urlPage > 3 && urlPage < lastPage ){
						pageInTheMiddle(urlPage, lastPage);
					}
				}
			}

			function pageLessThanFour() {
				for (var i = 0; i <listA.length; i++) {
					listA[i].innerHTML = i+1;
				}
			}

			function pageAlmostAtTheEnd(page, lastPage) {
				
				var number = 0;

				number = (lastPage - 3);

				for (var i = 0; i <listA.length; i++) {
					listA[i].innerHTML = number;
					number++;
				}
			}

			function pageInTheMiddle(page, lastPage) {
				
				var number = page - 2;

				for (var i = 0; i < listA.length; i++) {

					if(i === listA.length-1 ){
						listA[i].innerHTML = page + 1;
					}else{
						listA[i].innerHTML = number++;
					}
				}
			}
		}

		function refreshAfterUrlChange(page) {
			setUrlCompanyPage(page);

			updateMobilePagination();
			updateDesktopPagination();

			var pageUpdated = getUrlCompanyPage();
			
			setFocusPagination(pageUpdated);

			hidePartsOfThePagination(pageUpdated);

			var values = displayData(pageUpdated);

			loadCompanyData(values);
		}

		// Function receives data companies and creates all elements with the data information and appends all of then to "company data container"
		function loadCompanyData(data) {
			var companyDataContainer = home.getElementsByClassName("company-data-container ")[0];
			companyDataContainer.innerHTML = '';
			for (var i = 0; i < data.length; i++) {
					
				var companyDiv = document.createElement("DIV");
				var imageContainerDiv = document.createElement("DIV");
				var descriptionContainerDiv = document.createElement("DIV");
				var img = document.createElement("IMG");
				var p = document.createElement("P");
				
				companyDiv.classList.add("company");
				imageContainerDiv.classList.add("image-container");
				descriptionContainerDiv.classList.add("description-container");

				// Use a default value when the company doesn't has a logo
				if(!Array.isArray(data[i].images) ){
					img.src = data[i].images.orig.url;
				}else{
					img.src = 'http://motorradtoursperu.com/img/image-not-found.png';
				}

				p.innerHTML = data[i].description;

				descriptionContainerDiv.append(p);
				imageContainerDiv.append(img);

				companyDiv.append(imageContainerDiv);
				companyDiv.append(descriptionContainerDiv);
				
				companyDataContainer.append(companyDiv);
			}
		}

		return {

		};
	})();
});























