'use strict';

//requires
var gulp = require('gulp');
var nodemon = require('gulp-nodemon');

// config file
var config = require('../config');

/* this task restarts the server when some changes are saving  in the js files */
gulp.task('nodemon', ['build','watches'],function (cb) {
	
	var started = false;
	nodemon({
		script: config.express.file,
		ext: 'js json',
		watch: [
			config.express.file,
			config.express.route_folder,
			config.express.model_folder
		]
	}).on('start', function () {
		// to avoid nodemon being started multiple times
		// thanks @matthisk
		if (!started) {
			cb();
			started = true; 
		} 
	});
});