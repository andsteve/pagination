'use strict';

// requires
var gulp = require('gulp');

// config file
var config = require('../config');

/* this task copies the handlebar files on app folder to build folder */
gulp.task('copyJQuery', function() { 
	return gulp.src(config.appFolder.filePaths.JQuery)
		.pipe(gulp.dest(config.buildFolder.destFolders.js));
});