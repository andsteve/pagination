'use strict';

// requires
var gulp = require('gulp');

// config file
var config = require('../config');

/* this task copies the handlebar files on app folder to build folder */
gulp.task('copyHandlebars', function() { 
	return gulp.src(config.appFolder.filePaths.handlebars)
		.pipe(gulp.dest(config.buildFolder.destFolders.handlebars));
});