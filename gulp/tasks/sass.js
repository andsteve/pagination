'use strict';

//require
var gulp   = require('gulp');
var sass   = require('gulp-sass');

// config file
var config = require('../config');

/* this task converts scss files to css and moves the css files to buil folder */
gulp.task('sass', function() { 
	return gulp.src(config.appFolder.filePaths.sass)
	.pipe(sass().on('error', sass.logError))
	.pipe(gulp.dest(config.buildFolder.destFolders.css));
});