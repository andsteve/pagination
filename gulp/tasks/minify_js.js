'use strict';

// require
var gulp = require("gulp");
var minify = require('gulp-minify');
var ngmin = require('gulp-ngmin');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var ngAnnotate = require('gulp-ng-annotate');
var bytediff = require('gulp-bytediff');
var concat = require("gulp-concat");

// config file
var config = require('../config');


gulp.task('minify_js', ['lint'],function() {
	var source = config.appFolder.filePaths.js;

	return gulp.src(source)
		.pipe(sourcemaps.init())
		.pipe(concat('main.js', {newLine: ';'}))
		
		// Annotate before uglify so the code get's min'd properly.
		.pipe(ngAnnotate({
			// true helps add where @ngInject is not used. It infers.
			// Doesn't work with resolve, so we must be explicit there
			add: true
		}))

		.pipe(bytediff.start())
		.pipe(uglify({mangle: true}))
		.pipe(bytediff.stop())
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(config.buildFolder.destFolders.js));
});


