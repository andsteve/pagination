'use strict';

// requires 
var gulp = require('gulp');
var browserSync = require('browser-sync');

// config file
var config = require('../config');

/* this task inicialize browser-sync and run the server */
gulp.task('browser-sync', ['nodemon'], function() {
	var paths = [
					config.buildFolder.watchPaths.css,
					config.buildFolder.watchPaths.js,
					config.buildFolder.watchPaths.handlebars
				];

	browserSync.init(null, {
		proxy: "http://localhost:3000",
		files: paths,
		open: true,
		browser: "google chrome",
		port: 7000,
	});
});
