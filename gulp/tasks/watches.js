'use strict';

// requires
var gulp   = require('gulp');

// config file
var config = require('../config');

/* this task is listening some changes on scss, js, handlebars files */
gulp.task('watches', function () {
	gulp.watch(config.appFolder.filePaths.sass, ['sass']);
	gulp.watch(config.appFolder.filePaths.js, ['minify_js']);
	gulp.watch(config.appFolder.filePaths.handlebars, ['copyHandlebars']);
});