/**
 * Requires files
 */
var exphbs = require('express-handlebars');
var sessions = require('express-session');
var bodyParser = require('body-parser');
var flash = require('connect-flash');
var express = require('express');
var path = require('path');
var http = require('http');

var app = module.exports = express();

/* bodyParser configuration */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

/* routes configuration */
var home = require('./routes/home');

/* handlebars configuration */
app.set('views', path.join(__dirname,'build/views/renders'));
var handlebars = require('express-handlebars').create({
	defaultLayout:'main',
	extname: '.handlebars',
	layoutsDir:'build/views/layouts',
	partialsDir:'build/views/partials'
});
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

/* set public directory */
app.use(express.static(__dirname + '/build'));

app.use(sessions({
	secret: 'secret',
	saveUninitialized: true,
	resave: true
}));

/* allow flash messages */
app.use(flash());

// Global Variables
app.use(function (req, res, next) {
	res.locals.success_msg = req.flash('success_msg');
	res.locals.error_msg = req.flash('error_msg');
	res.locals.error = req.flash('error');
	next();
});

/* files to render */
var models = [home];

app.use('/', models);

/* if the route is not found it render to*/
app.use(function (req, res, next) {

	res.render('404');

})

app.set('port', process.env.PORT || 3000);

/**
 * Start Server
 */
http.createServer(app).listen(app.get('port'), function () {
	console.log('Express server listening on port ' + app.get('port'));
});