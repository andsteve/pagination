'use strict';

//EXPRESS
var express = require('express');
var router = express.Router();

//ROUTES

/* home route */
router.get('/' , home );

/* this function renter to home view */
function home(req,res){

	res.render('home');
};


module.exports = router;